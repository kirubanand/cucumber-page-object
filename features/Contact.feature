@SDFC @Contacts
Feature:Contacts
  In order test SFDC Contacts module
  As a tester
  And I should verify as per client requirement

  @create-contacts
  Scenario: Create contact
    Given I should navigate to salesforce Home page
    Then I should see salesforce home page succesfully
    When I click on Contacts tab from all tabs section
    Then I should see following:
      """
      Contacts
      Home
      """
    And I should see contacts page succesfully
    When I follow on "New" button
    Then I should see following:
      """
      Contact Edit
      New Contact
      """
    When I fill in "Last Name" with random "smoke-test-contacts-1-"
    And I fill in the following:
      | field           | value                  |
      | Account Name    | smoke-test-account-123 |
    And I click on "Save" button from top row