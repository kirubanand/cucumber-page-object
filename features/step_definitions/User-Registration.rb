
Given("I navigate to LMS user registration page") do
  visit " "
  sleep 30
end

When("I fill the user registration form and click submit") do |table|
  # table is a Cucumber::MultilineArgument::DataTable
    account_type = table.hashes[0]["Account_type"]
    @page=UserRegistrationpage.new
     @page.load
    case account_type
    when 'Corporate'
      @page.account_type_corporate.click
      sleep 50
    when 'Personal'
      @page.account_type_personal.click
      sleep 50
  end
  
end

Then("I should be on {string} page") do |string|
  
end

Then("I should see welcome message on header") do
  
end

Then("I logout") do
  
end