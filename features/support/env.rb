require "rubygems"
require "rspec"
require "rspec/expectations"
require "selenium-webdriver"
require 'Capybara'
require "capybara/cucumber"
require 'yaml'
require 'site_prism'
require 'capybara-screenshot/cucumber'


begin
  if ENV['Browser'] == 'firefox'
    Capybara.register_driver :selenium_firefox do |app|
      Capybara::Selenium::Driver.new(app, :browser => :firefox)
    end
    Capybara.default_driver         = :selenium_firefox
    Capybara.javascript_driver      = :selenium_firefox
    Capybara.run_server             = false
    Capybara.default_selector       = :css
    Capybara.default_max_wait_time = 90
  elsif ENV['Browser'] == 'ie'
    Capybara.register_driver :ie do |app|
      Capybara::Selenium::Driver.new(app, :browser => :ie)
    end
    Capybara.default_driver = :ie
    Capybara.default_max_wait_time = 90
  else
    Capybara.register_driver :chrome do |app|
      Capybara::Selenium::Driver.new(app, :browser => :chrome, :switches => %w[ --disable-extensions ])
    end
    Capybara.default_driver = :chrome
   # Capybara.asset_host = 'http://localhost:3000'
    Capybara.default_max_wait_time = 90
    Capybara.app_host = "https://training-lms-test.redhat.com"

#    Capybara::Screenshot.register_driver(:chrome) do |driver, path|
#     driver.browser.save_screenshot(Dir.pwd+"/screenshot.png")
#    end
#    Capybara.save_path = Dir.pwd + "/screenshots/"
#    Capybara::Screenshot.screenshot_and_save_page
    #Capybara::Screenshot.screenshot_and_open_image
  end
end

Before do |scenario|
  Capybara::Screenshot.final_session_name = nil
end

#def get_url
#  if ENV['UserRole'].include? 'QA'
#    Capybara.default_max_wait_time = 90
#    Capybara.app_host = "https://training-lms-qa.redhat.com"
#
#  elsif ENV['UserRole'].include? 'Dev'
#    Capybara.default_max_wait_time = 90
#    Capybara.app_host = "https://training-lms-test.redhat.com"
#
#  else
#    raise "Failed to match the #{ENV['UserRole']} name"
#  end
#end


#before hook to maximize a window
Before do
  page.driver.browser.manage.window.maximize
end

#After do |scenario|
#  if Capybara::Screenshot.autosave_on_failure && scenario.failed?
#    Capybara.using_session(Capybara::Screenshot.final_session_name) do
#      filename_prefix = Capybara::Screenshot.filename_prefix_for(:cucumber, scenario)
#
#      saver = Capybara::Screenshot.new_saver(Capybara, Capybara.page, true, filename_prefix)
#      saver.save
#      saver.output_screenshot_path
#
#      # Trying to embed the screenshot into our output."
#      if File.exist?(saver.screenshot_path)
#        require "base64"
#        #encode the image into it's base64 representation
#        image = open(saver.screenshot_path, 'rb') {|io|io.read}
#        saver.display_image
#        #this will embed the image in the HTML report, embed() is defined in cucumber
#        encoded_img = Base64.encode64(image)
#        embed(encoded_img, 'image/png;base64', "Screenshot of the error")
#      end
#    end
#  end
#end



#at_exit do
#  page.quit
#end


#After do |scenario|
#  if scenario.failed?
#    filename = "./Screenshot/failed_scenario_screenshots/#{scenario.name}_#{Time.now.strftime('%Y-%m-%d-%H-%M-%S')}.png"
#    File.open(filename,'wb') do |f|
#      f.write(Base64.decode64(page.driver.browser.screenshot_as(:base64)))
#      embed(filename, 'image/png;base64')
#    end
#  else
#    filename = "./Screenshot/success_scenario_screenshots/#{scenario.name}_#{Time.now.strftime('%Y-%m-%d-%H-%M-%S')}.png"
#    File.open(filename,'wb') do |f|
#      f.write(Base64.decode64(page.driver.browser.screenshot_as(:base64)))
#      embed(filename, 'image/png;base64')
#    end
#  end
#end