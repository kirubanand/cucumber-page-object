
class UserRegistrationpage < SitePrism::Page
  set_url "/sso/saml/auth/redhat"
  element :account_type_corporate, '#userTypeBUSINES'
  element :account_type_personal, '#userTypePERSONAL'
end
