@learner
Feature: User registration
  As a new subscriber on LMS
  I want to be able to register myself with different user modes [Corporate User, Personal User, Corporate User with double byte information, Personal User with double byte information ]
  So that I can get my LMS account

  Background:
    Given I navigate to LMS user registration page

   @user-registration
  Scenario Outline: Corporate user registration
    When I fill the user registration form and click submit
      | Account_type   | login   | email   | password   | company   | title   | greeting   | first_name   | last_name   | country   | address_line_1   | postal_code   | city   | state   | phone_number   | allow_receive_mail   |
      | <Account_type> | <login> | <email> | <password> | <company> | <title> | <greeting> | <first_name> | <last_name> | <country> | <address_line_1> | <postal_code> | <city> | <state> | <phone_number> | <allow_receive_mail> |
#    Then I should be on "Home" page
#    And I should see welcome message on header
#    And I logout

    Examples:
      | Account_type | login   | email   | password | company    | title        | greeting | country | address_line_1            | postal_code | city | state       | phone_number | allow_receive_mail | first_name | last_name |
      | Corporate    | default | default | redhat   | Automation | QA_Corporate | Mr.      | India   | Hadapsar                  | 412308      | Pune | Maharashtra | 9876543210   | false              | Automation | QA        |
#      | Personal     | default | default | redhat   | Automation | QA_Personal  | Mr.      | India   | Hadapsar                  | 412308      | Pune | Maharashtra | 9876543210   | false              | Automation | QA        |
#      | Personal     | default | default | redhat   | Automation | QA_Personal  | Mr.      | India   | 63 RENMIN LU, QINGDAO SHI | 412308      | Pune | Maharashtra | 9876543210   | false              | 紅帽         | 質量保證      |
#      | Corporate    | default | default | redhat   | Automation | QA_Corporate | Mr.      | India   | Hadapsar                  | 412308      | Pune | Maharashtra | 9876543210   | false              | 紅帽         | 質量保證      |