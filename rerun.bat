set "param1=%1"
setlocal EnableDelayedExpansion
rem if "!param1!"=="" ( set UserRole='CSMAdmin' ) else (set UserRole='%param1%')
set _my_datetime=%date%_%time%
set _my_datetime=%_my_datetime: =_%
set _my_datetime=%_my_datetime::=%
set _my_datetime=%_my_datetime:/=_%
set _my_datetime=%_my_datetime:.=_%
set _filename=Result\cucumber-Automation-Results-%_my_datetime%.html
cd /d %~dp0
cucumber features --tags @user-registration --format html --out results/cucumber-automation-results.html -f rerun --out rerun.txt || cucumber @rerun.txt